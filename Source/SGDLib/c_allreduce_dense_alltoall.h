#pragma once

//#include <stdio.h>      /* printf */
//#include <sys/time.h>   /* gettimeofday, timeval (for timestamp in microsecond) */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

/*
 * Dense AllToAll AllReduce
 */
template<class ValType> int c_allreduce_dense_alltoall(const ValType *sendbuf, ValType *recvbuf, unsigned dim, int prob) {

  /* initialize random seed: */
  srand (time(NULL));

  MPI_Comm comm = MPI_COMM_WORLD;
  int r, p;
  MPI_Comm_size(comm, &p);
  MPI_Comm_rank(comm, &r);

  if(p == 1) {
    memcpy(recvbuf, sendbuf, dim * sizeof(ValType));
    return MPI_SUCCESS;
  }

  ValType *tmpbuf = (ValType *)malloc(sizeof(ValType) * dim);

  int i; /* runner */
  int segsize, *segsizes, *segoffsets; /* segment sizes and offsets per segment (number of segments == number of nodes */
  int speer, rpeer; /* send and recvpeer */
  int mycount; /* temporary */
  segsizes = (int*)malloc(sizeof(int)*p);
  segoffsets = (int*)malloc(sizeof(int)*p);
  segsize = dim/p; /* size of the segments */
  if(dim%p != 0) segsize++;
  mycount = dim;
  segoffsets[0] = 0;
  for(i = 0; i<p;i++) {
    mycount -= segsize;
    segsizes[i] = segsize;
    if(mycount < 0) {
      segsizes[i] = segsize+mycount;
      mycount = 0;
    }
    if(i) segoffsets[i] = segoffsets[i-1] + segsizes[i-1];
    //if(!r) printf("count: %i, (%i) size: %i, offset: %i\n", count, i, segsizes[i], segoffsets[i]);
  }

  int selement = 0;
  int soffset = 0;
  int relement = r;
  int roffset = segoffsets[relement];
  int ext = sizeof(ValType);

  ValType *result = (ValType *)malloc(sizeof(ValType) * segsizes[relement]);

  int cnt = 0;
  int rnd = rand() % 100;
  if(rnd < prob) {
    cnt++;
    memcpy(tmpbuf+roffset, sendbuf+roffset, ext * segsizes[relement]);
  } else {
    for(i = 0; i < segsizes[relement]; ++i) {
        (tmpbuf+roffset)[i] = 0.0;
    }
  }

  int round = 0;
  /* first p-1 rounds are reductions */
  do {
    speer = (r+1+round + 2*p)%p;
    rpeer = (r-1-round + 2*p)%p;

    selement = (r+1+round + 2*p /*2*p avoids negative mod*/)%p; /* the element I am sending */
    soffset = segoffsets[selement];
    
    //struct timeval timer_usec; 
    //long long int timestamp_usec; /* timestamp in microsecond */
    //if (!gettimeofday(&timer_usec, NULL)) {
    //  timestamp_usec = ((long long int) timer_usec.tv_sec) * 1000000ll + 
    //    (long long int) timer_usec.tv_usec;
    //}
    //else {
    //  timestamp_usec = -1;
    //}
    //fprintf(stderr, "TLOG: %i;%i;%i;%lld\n", r, speer, (int)(sizeof(ValType) * segsizes[selement]),timestamp_usec);

    MPI_Sendrecv(sendbuf+soffset, sizeof(ValType) * segsizes[selement], MPI_BYTE, speer, 1, result, sizeof(ValType) * segsizes[relement], MPI_BYTE, rpeer, 1, comm, MPI_STATUS_IGNORE);
    
    //printf("[%i] round %i - sending %i\n", r, round, selement);
    //printf("[%i] round %i - receiving %i\n", r, round, relement);

    //printf("[%i] round %i - reducing %i\n", r, round, relement);
    int rnd = rand() % 100;
    if(rnd < prob) {
      cnt++;
    }
    for(i = 0; i < segsizes[relement]; ++i) {
      if(rnd < prob) {
        (tmpbuf+roffset)[i] = (tmpbuf+roffset)[i] + result[i];
      } else {
        (tmpbuf+roffset)[i] = (tmpbuf+roffset)[i];
      }
    }

    round++;
  } while(round < p-1);

  for(i = 0; i < segsizes[relement]; ++i) {
    (tmpbuf+roffset)[i] /= cnt;
  }

  selement = r;
  soffset = segoffsets[selement];

  rnd = rand() % 100;
  if(rnd < prob) {
    memcpy(recvbuf + soffset, tmpbuf+soffset, ext * segsizes[selement]);
  } else {
    //for(i = 0; i < segsizes[selement]; ++i) {
    //    (recvbuf + soffset)[i] = 0.0;
    //}
    if(recvbuf != sendbuf) {
      memcpy(recvbuf + soffset, sendbuf + soffset, ext * segsizes[selement]);
    }
  }

  round = 0;
  do {
    speer = (r+1+round + 2*p)%p;
    rpeer = (r-1-round + 2*p)%p;

    int relement = (r-1-round + 2*p /*2*p avoids negative mod*/)%p; /* the element that I receive from my neighbor */
    int roffset = segoffsets[relement];

    //struct timeval timer_usec; 
    //long long int timestamp_usec; /* timestamp in microsecond */
    //if (!gettimeofday(&timer_usec, NULL)) {
    //  timestamp_usec = ((long long int) timer_usec.tv_sec) * 1000000ll + 
    //    (long long int) timer_usec.tv_usec;
    //}
    //else {
    //  timestamp_usec = -1;
    //}
    //fprintf(stderr, "TLOG: %i;%i;%i;%lld\n", r, speer, (int)(sizeof(ValType) * segsizes[selement]),timestamp_usec);

    //printf("[%i] round %i receiving %i sending %i\n", r, round, relement, selement);
    MPI_Sendrecv(tmpbuf+soffset, sizeof(ValType) * segsizes[selement], MPI_BYTE, speer, 1, tmpbuf+roffset, sizeof(ValType) * segsizes[relement], MPI_BYTE, rpeer, 1, comm, MPI_STATUS_IGNORE);

    int rnd = rand() % 100;
    //fprintf(stderr, "Copy command run with prob: %i\n", rnd);
    if(rnd < prob) {
      memcpy(recvbuf + roffset, tmpbuf + roffset, ext * segsizes[relement]);
    } else {
      //for(i = 0; i < segsizes[relement]; ++i) {
      //  (recvbuf + roffset)[i] = 0.0;
      //}
      if(recvbuf != sendbuf) {
        memcpy(recvbuf + roffset, sendbuf + roffset, ext * segsizes[relement]);
      }
    }

    round++;  
  } while (round < p-1);

  free(tmpbuf);
  free(segsizes);
  free(segoffsets);
  free(result);

  return MPI_SUCCESS;
}
